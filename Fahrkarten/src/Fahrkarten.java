import java.util.Arrays;
import java.util.Scanner;

class Fahrkarten {
	static double doZahlenderBetrag(Scanner tastatur) {
		double zuZahlenderBetrag = 0;
		int Fahrkarte = 0;
		int Zahl = 1;
		String[] Bezeichner = new String[10];
		Bezeichner[0] = "Einzelfahrschein Berlin AB";
		Bezeichner[1] = "Einzelfahrschein Berlin BC";
		Bezeichner[2] = "Einzelfahrschein Berlin ABC";
		Bezeichner[3] = "Kurzstrecke";
		Bezeichner[4] = "Tageskarte Berlin AB";
		Bezeichner[5] = "Tageskarte Berlin BC";
		Bezeichner[6] = "Tageskarte Berlin ABC";
		Bezeichner[7] = "Kleingruppen-Tageskarte Berlin AB";
		Bezeichner[8] = "Kleingruppen-Tageskarte Berlin BC";
		Bezeichner[9] = "Kleingruppen-Tageskarte Berlin ABC";
		double[] Preis = new double[10];
		Preis[0] = 2.90;
		Preis[1] = 3.30;
		Preis[2] = 3.60;
		Preis[3] = 1.90;
		Preis[4] = 8.60;
		Preis[5] = 9.00;
		Preis[6] = 9.60;
		Preis[7] = 23.50;
		Preis[8] = 24.30;
		Preis[9] = 24.90;
		System.out.println("Welchen Fahrschein wollen sie haben?\n");
		for (String Bezeichnung : Bezeichner) {
			//System.out.printf("Noch zu zahlen: %.2g EURO", zuZahlenderBetrag - eingezahlterGesamtbetrag);
			System.out.print(Bezeichnung + " f�r ");
			if (Preis[(Zahl-1)] > 10) {
				System.out.printf("%.4g �", Preis[(Zahl-1)]);
				System.out.print(" ["+ Zahl + "]\n");
			}else {
				System.out.printf("%.3g �", Preis[(Zahl-1)]);
				System.out.print(" ["+ Zahl + "]\n");
			}
			Zahl++;
		}
		do {
			try {
				Fahrkarte = Integer.parseInt(tastatur.nextLine());
				switch (Fahrkarte) {
				case 1:
					zuZahlenderBetrag = Preis[(Fahrkarte - 1)];
					System.out.println("Ihre Auswahl: " + Bezeichner[(Fahrkarte - 1)]);
					break;
				case 2:
					zuZahlenderBetrag = Preis[(Fahrkarte - 1)];
					System.out.println("Ihre Auswahl: " + Bezeichner[(Fahrkarte - 1)]);
					break;
				case 3:
					zuZahlenderBetrag = Preis[(Fahrkarte - 1)];
					System.out.println("Ihre Auswahl: " + Bezeichner[(Fahrkarte - 1)]);
					break;
				case 4:
					zuZahlenderBetrag = Preis[(Fahrkarte - 1)];
					System.out.println("Ihre Auswahl: " + Bezeichner[(Fahrkarte - 1)]);
					break;
				case 5:
					zuZahlenderBetrag = Preis[(Fahrkarte - 1)];
					System.out.println("Ihre Auswahl: " + Bezeichner[(Fahrkarte - 1)]);
					break;
				case 6:
					zuZahlenderBetrag = Preis[(Fahrkarte - 1)];
					System.out.println("Ihre Auswahl: " + Bezeichner[(Fahrkarte - 1)]);
					break;
				case 7:
					zuZahlenderBetrag = Preis[(Fahrkarte - 1)];
					System.out.println("Ihre Auswahl: " + Bezeichner[(Fahrkarte - 1)]);
					break;
				case 8:
					zuZahlenderBetrag = Preis[(Fahrkarte - 1)];
					System.out.println("Ihre Auswahl: " + Bezeichner[(Fahrkarte - 1)]);
					break;
				case 9:
					zuZahlenderBetrag = Preis[(Fahrkarte - 1)];
					System.out.println("Ihre Auswahl: " + Bezeichner[(Fahrkarte - 1)]);
					break;
				case 10:
					zuZahlenderBetrag = Preis[(Fahrkarte - 1)];
					System.out.println("Ihre Auswahl: " + Bezeichner[(Fahrkarte - 1)]);
					break;
				default:
					throw new Exception();
				}
				// Fahrkarte = tastatur.nextLine();
				// if (Fahrkarte.equals("1")) {
				// zuZahlenderBetrag = 2.90;
				// } else if (Fahrkarte.equals("2")) {
				// zuZahlenderBetrag = 8.60;
				// } else if (Fahrkarte.equals("3")) {
				// zuZahlenderBetrag = 23.50;
				// } else {
				// throw new Exception();
				// }
				break;
			} catch (Exception e) {
				System.out.println("Keine der vorgegeben Zahlen wurde angegeben.");
			}
		} while (!(Fahrkarte == 1 || Fahrkarte == 2 || Fahrkarte == 3));
		return zuZahlenderBetrag;
	}

	static int fahrkartenbestellungErfassen(Scanner tastatur) {
		int Fahrkarten = 2;
		do {
			try {
				System.out.println("Wie viele Fahrkarten sollen gedruckt werden? ");
				Fahrkarten = Integer.parseInt(tastatur.nextLine());
				if (Fahrkarten > 10) {
					System.out.println("Nicht mehr als 10 Karten.");
				}
			} catch (Exception e) {
				System.out.println("Die eingabe ist keine Zahl!");
			}
		} while (!(Fahrkarten <= 10 && Fahrkarten >= 1));
		return Fahrkarten;

	}

	static double fahrkartenBezahlen(double zuZahlenderBetrag, Scanner tastatur) {
		double eingeworfeneMuenze;
		double eingezahlterGesamtbetrag = 0.0;
		try {
			while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
				if ((zuZahlenderBetrag - eingezahlterGesamtbetrag) < 0.10) {
					System.out.printf("Noch zu zahlen: %.1g EURO", zuZahlenderBetrag - eingezahlterGesamtbetrag);
				} else if ((zuZahlenderBetrag - eingezahlterGesamtbetrag) < 1) {
					System.out.printf("Noch zu zahlen: %.2g EURO", zuZahlenderBetrag - eingezahlterGesamtbetrag);

				} else {
					System.out.printf("Noch zu zahlen: %.3g EURO", zuZahlenderBetrag - eingezahlterGesamtbetrag);
				}
				// System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag -
				// eingezahlterGesamtbetrag));
				System.out.println("\nEingabe (mind. 5Ct, hoechstens 2 Euro): ");
				eingeworfeneMuenze = Double.parseDouble(tastatur.nextLine());
				if (eingeworfeneMuenze > 2) {
					System.out.println("Sie Haben zu Viel Geld eingeworfen! Ihre " + eingeworfeneMuenze
							+ " Euro wurde wieder ausgeworfen.");

				} else {

					eingezahlterGesamtbetrag += eingeworfeneMuenze;
				}
			}

		} catch (Exception e) {
			System.out.println("Die eingabe ist keine Zahl!");
		}
		return (eingezahlterGesamtbetrag - zuZahlenderBetrag);

	}

	static void rueckgeldAusgeben(double rueckgabebetrag) {
		if (rueckgabebetrag > 0.0) {
			rueckgabebetrag = rueckgabebetrag * 100;
			rueckgabebetrag = Math.round(rueckgabebetrag);
			rueckgabebetrag = rueckgabebetrag / 100;
			System.out.println("Der Rueckgabebetrag in Hoehe von " + rueckgabebetrag + " EURO");
			System.out.println("wird in folgenden Muenzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) // 2 EURO-Muenzen
			{
				System.out.println("2 EURO");
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) // 1 EURO-Muenzen
			{
				System.out.println("1 EURO");
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-Muenzen
			{
				System.out.println("50 CENT");
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.2) // 20 CENT-Muenzen
			{
				System.out.println("20 CENT");
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.1) // 10 CENT-Muenzen
			{
				System.out.println("10 CENT");
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-Muenzen
			{
				System.out.println("5 CENT");
				rueckgabebetrag -= 0.05;
			}
			while (rueckgabebetrag >= 0.01) {
				rueckgabebetrag = muenzeAusgeben(1, "CENT", rueckgabebetrag);
			}
		}
	}

	static double muenzeAusgeben(int betrag, String einheit, double rueckgabebetrag) {

		System.out.println(betrag + " " + einheit);
		rueckgabebetrag -= 0.01;
		rueckgabebetrag = rueckgabebetrag * 100;
		rueckgabebetrag = Math.round(rueckgabebetrag);
		rueckgabebetrag = rueckgabebetrag / 100;
		return rueckgabebetrag;
	}

	static void Fahrscheinausgabe(int Fahrkarten) {
		if (Fahrkarten > 1) {
			System.out.println("\nIhre " + Fahrkarten + " Fahrkarten werden ausgegeben");
		} else {
			System.out.println("\nIhr Fahrschein wurde ausgegeben");
		}
	}

	static void doWarte() {
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

	}

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		do {

			double zuZahlenderBetrag = doZahlenderBetrag(tastatur);
			int Fahrkarten = fahrkartenbestellungErfassen(tastatur);
			zuZahlenderBetrag = zuZahlenderBetrag * Fahrkarten;

			// Geldeinwurf
			// -----
			double rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);
			// -----

			// Abwarten
			// -----
			doWarte();
			// -----

			// Fahrscheinausgabe
			// -----
			Fahrscheinausgabe(Fahrkarten);
			// -----

			// Rueckgeldberechnung und -Ausgabe
			// -----
			rueckgeldAusgeben(rueckgabebetrag);
			// -----

			// Beenden

			System.out.println("Wollen sie einen weiteren Fahrschein?");
			String Frage = tastatur.nextLine();
			if (Frage.equals("ja") || Frage.equals("Ja")) {
				System.out.println("Okay");
			} else {
				System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
						+ "Wir wuenschen Ihnen eine gute Fahrt.");
				break;
			}
		} while (true);
		tastatur.close();
		System.out.println("Hallo");
		System.exit(1);
	}
}
