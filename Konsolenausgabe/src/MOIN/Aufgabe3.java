package MOIN;

public class Aufgabe3 {

	public static void main(String[] args) {
	
		double Celsius1 = -28.8889;
		double Celsius2 = -23.3333;
		double Celsius3 = -17.7778;
		double Celsius4 = -6.6667;
		double Celsius5 = -1.1111;
		
		System.out.printf("%-12s | %10s\n", "Fahrenheit", "Celsius");
		for (int i = 0; i <= 25; i++) {
			System.out.print("-");
			if ( i == 25) {
				System.out.println("");
			}
		}
		System.out.printf("%-12s | %10.2f\n", -20, Celsius1);
		System.out.printf("%-12s | %10.2f\n", -10, Celsius2);
		System.out.printf("+%-11s | %10.2f\n", +0, Celsius3);
		System.out.printf("+%-11s | %10.2f\n", +20, Celsius4);
		System.out.printf("+%-11s | %10.2f\n", +30, Celsius5);
	}

}