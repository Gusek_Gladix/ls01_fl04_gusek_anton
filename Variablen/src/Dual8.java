
	import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Scanner;

	public class Dual8 {

		public static void main(String[] args) {
		    // Erstellt mir einen Scanner mit namen "Eingabe" f�r die Eingabe des Anwenders.
			Scanner Eingabe = new Scanner(System.in);
			// Erstellt den boolean "neustart" um am Ende des Programms f�r einen Neustart zu Fragen.
			boolean neustart = false;
			// Eine Do while Schleife, mit mindestens einen Durchlauf.
			do {
				// Abfrage welcher Rechner abgerufen werden soll.
				System.out.print("M�chten sie eine Bin�rzahl umwandeln(1) oder eine Zahl (2) ?");
				// Speicherung der Anwender Antwort in den String "Antwort".
				String Antwort = Eingabe.nextLine();
				// Wenn die Antwort "1" ist, aktiviere den Bin�r in Dezimal Rechner.
				if (Antwort.equals("1")) {
						// erstelle ben�tigte Variablen.
					    // Die variable Potenz wird f�r das Potenzieren benutzt.
						int Potenz = -1;
						// Hier wird das Ergebnis gespeichert.
						int Ergebnis = 0;
						// Die Eingabe des Anwenders.
						String EingabeDualZahl = "";
						// Die Variable ob es sich um eine Gerade oder Ungerade Zahl handelt.
						boolean IstEsGerade = false;
						
						// Pr�fe ob es eine Bin�rzahl ist und ob die Zahl Gerade/Ungerade ist.
						try {
							// Eingabe der Bin�rZahl
							System.out.print("Bitte gebe eine Bin�rzahl an: ");
							EingabeDualZahl = Eingabe.nextLine();
							// Gehe durch jeden Buchstaben der Eingabe
							for ( char c : EingabeDualZahl.toCharArray()){
								// Pr�fe ob die Zahl keine Bin�rzahl ist.
								if (Character.toString(c).matches("2|3|4|5|6|7|8|9")){
									// Ist keine Bin�rzahl, werfe eine Fehlermeldung.
									throw new Exception ("Ist keine Bin�rzahl");
								}
								//Die Zahl Ziffer Ist eine Bin�rzahl, Erh�he meine Potenz um 1.
								Potenz++;
								// �berpr�ft ob die Letzte Ziffer der Zahl eine 0 ist.
								if (Character.toString(c).matches("0")) {
									// Bei einer null , ist die Zahl die Ausgerechnet wird gerade und somit ist der Wert True;
									IstEsGerade = true;
								}
								else {
									// Bei einer eins, ist die Zahl die Ausgerechnet wird ungerade und somit ist der Wert False;
									IstEsGerade = false;
								}
							}
						}
						// Wenn es keine Bin�rzahl ist, gebe die Fehlermeldung aus und schlie�e das Programm.
						catch(Exception e)
						{
							System.out.println(e);
							// Schlie�t das Programm.
							System.exit(1);
						}
						
						// Geht erneut durch Jede Ziffer in der Bin�rzahl.
						for ( char c : EingabeDualZahl.toCharArray()){
							// Moduliert Die Ziffer mit 2 und Multipliziert das Ergebnis mit 2 Hoch der Potenz.
							Ergebnis += Math.pow(((c % 2)/* 0 oder 1 */ * 2),Potenz/* Hoch */);
							// Senke Potent um 1.
							Potenz--;
							
						}
						// Wenn die Variable "IstEsGerade" Wahr ist, Senke mein Ergebnis durch 1.
						if (IstEsGerade) {
							Ergebnis--;
						}
						// Gebe mir mein Ergebnis aus.
						System.out.println("Die Umgewandelte Bin�rzahl ist: " + Ergebnis);
				}
				// Wenn die Antwort 2 ist, Aktiviere den Dezimal in Bin�rzahl Rechner.
				else if (Antwort.equals("2")) {
						// Erstellung der ben�tigten variablen Listen und variablen.
						// "ZwischenBinearZahl" ist f�r die zwischen Speicherung verantwortlich.
						ArrayList<String> ZwischenBinearZahl = new ArrayList<String>();
						// Hier wird das Ergebnis gespeichert.
						ArrayList<String> Ergebnis = new ArrayList<String>();
						// Eingabe der Zahl.
						System.out.print("Bitte gib deine Zahl ein: ");
						BigInteger EingabeDezimalZahl = new BigInteger(Eingabe.nextLine());
						// Solange die eingabe gr��er ist als 0 // Eingabe > 0
						while (EingabeDezimalZahl.compareTo(new BigInteger("0")) == 1) {
							// F�ge das das Ergebnis von (EingabeDezimalZahl % 2) in die Zwischenzahl.
							ZwischenBinearZahl.add(EingabeDezimalZahl.mod(new BigInteger("" + 2)).toString());
							// Speichere das Ergebnis von (EingabeDezimalZahl / 2) in die Eingabe.
							EingabeDezimalZahl = EingabeDezimalZahl.divide(new BigInteger("" + 2));
						}
						// Gehe durch jeden Index in der Liste und Speichere die Zahl in Ergebnis. Hier wird von hinten nach vorne gez�hlt.
						for (int i = ZwischenBinearZahl.size(); i > 0 ; i--) {
							Ergebnis.add(ZwischenBinearZahl.get(i-1));
						}
						// Ausgabe des Ergebnisses.
						System.out.print("Die Umgewandelte Dezimalzahl ist: ");
						for (String i : Ergebnis) {
							
							System.out.print(i);
						}
						System.out.println("");

				}
				// Die Eingabe war nich 1 oder 2. 
				else {
					System.out.println("Falsche Angabe.");
					// Beenden des Programmes
					System.exit(1);
				}
				// Abfrage ob die Schleife nochmal durchlaufen werden soll.
				System.out.print("Wollen sie nochmal etwas umwandeln? ");
				String Nochmal = Eingabe.nextLine();
				if (Nochmal.contains("ja")) {
					neustart = true;
				}
				else {
					neustart = false;
				} 
				
			}
			while (neustart);
			// Schleife wurde beendet.
			// Gebe lade balken aus und schlie�e das Programm.
			for(int i = 0; i<=5 ; i++) {
				System.out.print("=");
				try {
					Thread.sleep(550);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("\nProgramm beendet.");
			Eingabe.close();
			
		}

	}
	



	

//		 

		
		//Scanner eingabe = new Scanner(System.in);//Scanner zur Speicherung der Eingabe
//		System.out.println("Bitte gib eine Dualzahl ein!");//Aufforderung zur Eingabe
//		
//		int DualZahl = eingabe.nextInt();//Eingabe der bin�ren Zahl wird gespeichert
//		int Zaehler=0;//Anzahl der Stellen mit Startwert 0
//		int Zwischensumme=0;//Dezimalzahl welche ermittelt wird
//		int restWert=0;//Restwert als Zwischenergebnis
//		
//		while (DualZahl !=0){
//			restWert=DualZahl % 10;//Restwert nach Kommaverschiebung aus letzten Durchlauf
//			Zwischensumme += (int)(restWert*(Math.pow(2, Zaehler)));//Summierte Zwischenwerte	
//			DualZahl /= 10;//neue Kommaverschiebung
//			Zaehler++;//Anzahl der Stellen wird je Durchlauf um 1 erh�ht
//		}
//		System.out.println(Zwischensumme);//Ausgabe der Dezimalzahl
//		eingabe.close();
//	}
//
//}