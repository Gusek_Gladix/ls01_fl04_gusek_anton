import java.util.Scanner;
public class Fahrsimulator {
	
	static Scanner tastatur = new Scanner(System.in);
	
	static double beschleunige(double v ,double dv) {
		if ((v + dv) >= 130) {
			return 130;
		}
		else if ((v + dv) <= 0){
			return 0;
		}
		return (v + dv);
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean weiter_fahren = false;
		double Geschwindigkeit = 0;
		String frage;
		System.out.println("Der Mercedes wurde Gestartet und steht noch. ");
		
		do  {
			System.out.print("Wie viel m�chtest du Beschleunigen? ");
			double dv = Double.parseDouble(tastatur.nextLine());
			
			
			Geschwindigkeit = beschleunige(Geschwindigkeit, dv);
			if (dv < 0) {
				System.out.println("Der Mercedes bremst auf: " + Geschwindigkeit);
			}else if (Geschwindigkeit == 130) {
				System.out.println("Der Mercedes f�hrt mit Maximal Geschwindigkeit: " + Geschwindigkeit);
			}else {
				System.out.println("Der Mercedes beschleunigt auf: " + Geschwindigkeit);
			}
			System.out.print("M�chtest du weiter Beschleunigen? ");
			frage = tastatur.nextLine();
			if ("ja".equals(frage)) {
				weiter_fahren = true;
			}
		}
		while(weiter_fahren);
	}
}

