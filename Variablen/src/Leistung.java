import java.util.Scanner;

public class Leistung {
	
	public static void main(String[] args) {
		// P = u * a
		// i = P / u
		// R = u / i
		Scanner myScanner = new Scanner(System.in);
		double einzelleistung = 0; // watt
		double gesamtleistung = 0;
		double gesamtstromstaerke = 0;
		final double netzspannung = 230.0;
		final double maxStromstaerke = 16.0;
		int anzahlPCs;
		int anzahlStromkreise = 1;
		
		System.out.print("\nLeistung eines PC-Arbeitsplatzes [in Watt]: ");
		einzelleistung = myScanner.nextDouble();
		System.out.print("Anzahl der PC-Arbeitspl�tze: ");
		anzahlPCs = myScanner.nextInt();
		
		
		// Berechnung der erforderlichen Stromst�rke und der
		
		gesamtleistung = einzelleistung * anzahlPCs; // P gesamt
		gesamtstromstaerke = gesamtleistung / netzspannung;
		// Anzahl der ben�tigten Stromkreise:
		if (gesamtstromstaerke >= maxStromstaerke) {
			anzahlStromkreise ++;
		}
		System.out.println("Gesamtleistung:  "+ gesamtleistung + "W");
		System.out.println("Gesamtstromst�rke:  " + gesamtstromstaerke + "A");
		System.out.println("Anzahl der Stromkreise:  "+ anzahlStromkreise);
		myScanner.close();
	}
}
