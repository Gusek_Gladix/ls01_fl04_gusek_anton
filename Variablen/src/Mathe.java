import java.util.Scanner;

public class Mathe {
	static Scanner tastatur = new Scanner(System.in);
	
	static double quadrat(double x) {
		x = Math.pow(x, 2);
		return x;
	}
	static double hypotenuse(double kathete1, double kathete2) {
		double ergebnis ;
		
		ergebnis = Math.sqrt(quadrat(kathete1) + quadrat(kathete2));
		
		return ergebnis;
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.print("Bitte gebe die Erste Kathete ein: ");
		double k1 = Double.parseDouble(tastatur.nextLine());
		System.out.print("Bitte gebe nun die Zweite ein: ");
		double k2 = Double.parseDouble(tastatur.nextLine());
		System.out.println("Das Ergebnis ist: " + hypotenuse(k1,k2));
	}

}
